# CIGeoE Identify Dangles

This plugin merge multiple lines by coincident vertices and with the same attribute names and values.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_merge_multiple_lines” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Merge Multiple Lines”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_merge_multiple_lines” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - In the “Layers Panel” select the “layer” and put it into editing.

![ALT](/images/image1.png)

2 - Example: four lines.

![ALT](/images/image2.png)

3 - Click on the plugin icon, press the left mouse button and drag the mouse to create the rectangle over the lines to join and drop. The tool will join all lines that have coincident vertices.

![ALT](/images/image3.png)

4 - At the end of the procedure, it displays a message (blue) if successful, otherwise it displays a “MessageBox” with information.
![ALT](/images/image4.png)

5 - Click on plugin icon to disable.

6 - Result:

![ALT](/images/image5.png)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
