# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=CIGeoE Merge Multiple Lines
qgisMinimumVersion=3.0
description=Merge multiple lines by coincident vertices and with the same attribute names and values.
version=1.0
author=Centro de Informação Geoespacial do Exército
email=igeoe@igeoe.pt

about=Merge multiple lines by coincident vertices and with the same attribute names and values.

tracker=https://gitlab.com/cigeoe/cigeoe_merge_multiple_lines/-/issues
repository=https://gitlab.com/cigeoe/cigeoe_merge_multiple_lines
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=3d,vector,layers

homepage=https://gitlab.com/cigeoe/cigeoe_merge_multiple_lines
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

Category of the plugin: Raster, Vector, Database or Web
# category=

# If the plugin can run on QGIS Server.
server=False

